/*
[{
  name: 'The name of the task'
  dockerImage: 'biodataprocessor/bcgsc-mirna'
  exec: 'Docker Entry Point'
  args: [arguments]
}]
options => {concurrency: 1, cpus: 1, memory: '2GB', nodes: 1, autoStart: TRUE, updateInterval: 20000, watchFileTimeout: 30000}
*/
module.exports = {
  dockerWrapper: require('./lib/docker_wrapper'),
  runProcess: require('./lib/_runProcess.js')
};
