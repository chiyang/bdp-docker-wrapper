const child_process = require("child_process"),
  path = require("path"),
  fse = require("fs-extra"),
  yaml = require("js-yaml"),
  chownr = require('chownr');
const pathMapping = function(oldPath, refPath, mappingRef) {
  mappingRef = mappingRef || "/data/";
  if (oldPath && refPath) {
    oldPath = oldPath.replace(/\\ /g, " ").replace(/\\\\/g, "/");
    refPath = refPath.replace(/\\ /g, " ").replace(/\\\\/g, "/");
    let relativePath = path.relative(refPath, oldPath);
    relativePath = relativePath.split(path.sep).join("/");
    if (relativePath.indexOf("..") == 0) {
      // process.stderr.write('Path mapping error: ' + oldPath + ' mapped with ' + refPath + '\n');
      return undefined;
    } else {
      return mappingRef + relativePath;
    }
  } else {
    // process.stderr.write('Path mapping error: ' + oldPath + ' mapped with ' + refPath + '\n');
    return undefined;
  }
};

const chownrAsync = (path, uid, gid) => {
  return new Promise((resolve, reject) => {
    chownr(path, uid, gid, (err) => {
      return err ? reject(err) : resolve();
    });
  })
}

let titleGraph =
  "==============================================================================================================================\n";
titleGraph += `
 ______     __     ______        _____     ______     ______   ______    
/\\  == \\   /\\ \\   /\\  __ \\      /\\  __-.  /\\  __ \\   /\\__  _\\ /\\  __ \\   
\\ \\  __<   \\ \\ \\  \\ \\ \\/\\ \\     \\ \\ \\/\\ \\ \\ \\  __ \\  \\/_/\\ \\/ \\ \\  __ \\  
 \\ \\_____\\  \\ \\_\\  \\ \\_____\\     \\ \\____-  \\ \\_\\ \\_\\    \\ \\_\\  \\ \\_\\ \\_\\ 
  \\/_____/   \\/_/   \\/_____/      \\/____/   \\/_/\\/_/     \\/_/   \\/_/\\/_/                                                                                                        
             ______   ______     ______     ______     ______     ______     ______     ______     ______    
            /\\  == \\ /\\  == \\   /\\  __ \\   /\\  ___\\   /\\  ___\\   /\\  ___\\   /\\  ___\\   /\\  __ \\   /\\  == \\   
            \\ \\  _-/ \\ \\  __<   \\ \\ \\/\\ \\  \\ \\ \\____  \\ \\  __\\   \\ \\___  \\  \\ \\___  \\  \\ \\ \\/\\ \\  \\ \\  __<   
             \\ \\_\\    \\ \\_\\ \\_\\  \\ \\_____\\  \\ \\_____\\  \\ \\_____\\  \\/\\_____\\  \\/\\_____\\  \\ \\_____\\  \\ \\_\\ \\_\\ 
              \\/_/     \\/_/ /_/   \\/_____/   \\/_____/   \\/_____/   \\/_____/   \\/_____/   \\/_____/   \\/_/ /_/ 
                                                             
`;
titleGraph +=
  "==============================================================================================================================\n";
const dockerWrapper = function() {
  /**
   * Changing user id
   * TODO: Note: changing user/group id does not change the uid and gid of all existed files. (e.g. /opt/conda/lib/R/library)
   * https://muffinresearch.co.uk/linux-changing-uids-and-gids-for-user/
   * Do we need to replace all uid/gid of biodocker to the desired
   */
  let uid, gid;
  if (process.env.HOST_USER_ID) {
    uid = isNaN(Number(process.env.HOST_USER_ID)) ? undefined : Number(process.env.HOST_USER_ID);
    gid = isNaN(Number(process.env.HOST_USER_GROUP)) ? uid : Number(process.env.HOST_USER_GROUP)
    /**
     * Remove gid 20 (dialout) because MacOS users usually have this gid.
     */
    let removeGid20 = child_process.spawnSync("groupdel", ["dialout"], {
      encoding: "utf8"
    });
    if (removeGid20.stderr) {
      process.stderr.write(
        "Can not remove the original gid 20 (for dialout)\n"
      );
      process.stderr.write(removeGid20.stderr);
      process.exit(1);
    }
    let changeID = child_process.spawnSync(
      "usermod",
      ["-u", uid, "biodocker"],
      { encoding: "utf8" }
    );
    if (changeID.stderr) {
      process.stderr.write("Can not change user id to " + uid + "\n");
      process.stderr.write(changeID.stderr);
      process.exit(1);
    }
    changeID = child_process.spawnSync("groupmod", ["-g", gid, "biodocker"], {
      encoding: "utf8"
    });
    if (changeID.stderr) {
      process.stderr.write("Can not change group id to " + gid + "\n");
      process.stderr.write(changeID.stderr);
      process.exit(1);
    }
  }
  let RunningProcess = undefined;
  let projectFolder = undefined;
  if (process.env.PROJECT_FOLDER) {
    projectFolder = process.env.PROJECT_FOLDER.replace(/\\/g, "\\\\");
  }
  process.stderr.write(
    "PROJECT_FOLDER = " + (projectFolder || "(unset)") + ".\n"
  );

  let packageFolder = undefined;
  if (process.env.PACKAGE_FOLDER) {
    packageFolder = process.env.PACKAGE_FOLDER.replace(/\\/g, "\\\\");
  }
  process.stderr.write(
    "PACKAGE_FOLDER = " + (packageFolder || "(unset)") + ".\n"
  );
  const runProcess = function(exec, args, processName) {
    return new Promise((resolve, reject) => {
      process.stderr.write(
        "[" + new Date().toString() + "] " + processName + " Starting.\n"
      );
      process.stderr.write("[command] " + exec + " " + args.join(" ") + "\n");
      // process.stderr.write('[HOST_USER_ID] ' + process.env.HOST_USER_ID + '\n');
      // process.stderr.write('[HOST_USER_GROUP] ' + process.env.HOST_USER_GROUP + '\n');
      RunningProcess = child_process.spawn(exec, args, {
        cwd: process.cwd(),
        shell: true,
        env: process.env,
        uid: uid,
        gid: gid
      });
      RunningProcess.stderr.pipe(process.stderr);
      RunningProcess.stdout.pipe(process.stdout);
      RunningProcess.on("error", err => {
        process.stderr.write(
          "[" + new Date().toString() + "] " + processName + " has errors.\n"
        );
        process.stderr.write(JSON.stringify(err));
        reject(processName + " failed.");
      });
      RunningProcess.on("exit", code => {
        if (code !== 0) {
          process.stderr.write(
            "[" + new Date().toString() + "] " + processName + " Aborted.\n"
          );
          reject(processName + " Aborted.");
        } else {
          process.stderr.write(
            "[" + new Date().toString() + "] " + processName + " Finished.\n"
          );
          resolve(processName + " Finished.");
        }
      });
    });
  };
  process.on("SIGINT", () => {
    if (RunningProcess) {
      RunningProcess.kill();
    }
    console.log("Recieving SIGINT signal.");
    process.exit(1);
  });

  process.on("SIGTERM", () => {
    if (RunningProcess) {
      RunningProcess.kill();
    }
    console.log("Recieving SIGTERM signal.");
    process.exit(1);
  });
  const packageInstall = async function() {
    await fse.remove("/config/scripts");
    await fse.remove("/config/db");
    await fse.remove("/config/client");
    await fse.ensureDir("/config/scripts");
    await fse.ensureDir("/config/db");
    await fse.ensureDir("/config/client");
    await fse.ensureDir("/config/configs");
    process.stdout.write("Copying external scripts...");
    await fse.copy("/home/biodocker/bdp-package/scripts", "/config/scripts");
    process.stdout.write("ok\n");
    process.stdout.write("Copying database files...");
    await fse.copy("/home/biodocker/bdp-package/db", "/config/db");
    process.stdout.write("ok\n");
    process.stdout.write("Copying client files...");
    await fse.copy("/home/biodocker/bdp-package/client", "/config/client");
    process.stdout.write("ok\n");
    process.stdout.write("Copying config files...");
    await fse.copy("/home/biodocker/bdp-package/configs", "/config/configs", {
      overwrite: false
    });
    if (uid && gid) {
      await chownrAsync('/config', uid, gid);
    }
    process.stdout.write("ok\n");
  };
  return {
    projectFolder: projectFolder,
    packageFolder: packageFolder,
    pathMapping: pathMapping,
    actionWrapping: function(validActions) {
      // process.stderr.write(titleGraph);
      const action = process.argv[2];
      const cmd = validActions[action];
      if (action === "install") {
        process.stdout.write(titleGraph);
        process.stderr.write(titleGraph);
        const welcome =
          "You are now installing the package with a docker wrapper.\n" +
          "Please wait while we are moving files to the Bio-Data Processor\n";
        process.stdout.write(welcome);
        process.stderr.write(welcome);
        packageInstall()
          .then(() => {
            const finish = "Installing finished\n";
            process.stdout.write(finish);
            process.stderr.write(finish);
          })
          .catch(e => {
            console.log(e);
            const failed = "Installing failed\n";
            process.stdout.write(failed);
            process.stderr.write(failed);
          });
      } else {
        if (!validActions[action]) {
          process.stderr.write("Unknown command: " + action + "\n");
          process.exit(1);
        }
        runProcess(cmd.exec, cmd.args, action)
          .then(msg => {
            process.stderr.write(msg + "\n");
            process.stdout.write(msg + "\n");
          })
          .catch(msg => {
            process.stderr.write(msg + "\n");
            process.stdout.write(msg + "\n");
            process.exit(1);
          });
      }
    },
    readActionFromFile: function(filePath) {
      const validActions = {};
      const actionObject =
        yaml.safeLoad(fse.readFileSync(filePath, "utf8")) || {};
      const actions = Object.keys(actionObject);
      const processArgs = process.argv;
      for (const action of actions) {
        const taskObj = actionObject[action];
        validActions[action] = { exec: taskObj["exec"], args: [] };
        const theArgs = validActions[action].args;
        if (taskObj.script) {
          theArgs.push(taskObj.script);
        }
        if (taskObj.args) {
          if (taskObj.args === "auto") {
            theArgs.push.apply(theArgs, processArgs.slice(3).map(arg => ('"' + arg.replace(/\"/g, '\\\"') + '"')));
          } else {
            for (const argObj of taskObj.args) {
              let theArgValue = null;
              
              if (argObj.valueFrom && argObj.valueFrom.indexOf("argv.") === 0) {
                const argIndex = parseInt(argObj.valueFrom.split(".")[1]);
                if (argIndex >= 0) {
                  if (argObj.type === "file") {
                    theArgValue = this.pathMapping(
                      processArgs[argIndex + 3],
                      argObj.mapRef === "config"
                        ? this.packageFolder
                        : this.projectFolder,
                      argObj.mapRef === "config" ? "/config/" : "/data/"
                    );
                  } else if (argObj.type === "boolean") {
                    if (
                      processArgs[argIndex + 3] === "True" ||
                      processArgs[argIndex + 3] === "true" ||
                      processArgs[argIndex + 3] === "T"
                    ) {
                      theArgValue =
                        argObj.staticValue && argObj.staticValue.trim
                          ? argObj.staticValue.trim()
                          : "true";
                    } else {
                      theArgValue = null;
                    }
                  } else {
                    theArgValue = processArgs[argIndex + 3];
                  }
                }
              } else if (
                argObj.type !== "boolean" &&
                argObj.staticValue != null &&
                String(argObj.staticValue).trim()
              ) {
                theArgValue = String(argObj.staticValue).trim();
              }
              if (
                theArgValue !== null ||
                theArgValue !== undefined ||
                theArgValue !== ""
              ) {
                if (argObj.prefix != null) {
                  if (argObj.prefix.match(/.*\ +$/)) {
                    theArgValue = argObj.prefix + '"' + (theArgValue && theArgValue.replace ? theArgValue.replace(/\"/g, "\\\"") : theArgValue) + '"';
                  }else {
                    theArgValue = '"' + (argObj.prefix + theArgValue).replace(/\"/g, "\\\"") + '"';
                  }
                }else {
                  theArgValue = '"' + (theArgValue && theArgValue.replace ? theArgValue.replace(/\"/g, '\\\"') : theArgValue) + '"';
                }
                theArgs.push(theArgValue);
              }
            }
          }
        }
      }
      this.actionWrapping(validActions);
    }
  };
};

module.exports = dockerWrapper;
