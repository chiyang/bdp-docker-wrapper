const child_process = require('child_process');
module.exports = function(exec, args, processName){
  return new Promise((resolve, reject) => {
    process.stderr.write('[' + new Date().toString() + '] ' + processName + ' Starting.\n');
    process.stderr.write('[command] ' + exec + ' ' + args.join(' ') + '\n');
    // process.stderr.write('[HOST_USER_ID] ' + process.env.HOST_USER_ID + '\n');
    // process.stderr.write('[HOST_USER_GROUP] ' + process.env.HOST_USER_GROUP + '\n');
    const RunningProcess = child_process.spawn(exec, args, {cwd: process.cwd(), shell: true, env: process.env, 
      uid: isNaN(Number(process.env.HOST_USER_ID)) ? undefined : Number(process.env.HOST_USER_ID),
      gid: isNaN(Number(process.env.HOST_USER_GROUP)) ? undefined : Number(process.env.HOST_USER_GROUP),
    });
    RunningProcess.stderr.pipe(process.stderr);
    RunningProcess.stdout.pipe(process.stdout);
    RunningProcess.on('error', (err) => {
      process.stderr.write('[' + new Date().toString() + '] ' + processName + ' has errors.\n');
      process.stderr.write(JSON.stringify(err));
      reject(processName + ' failed.');
    });
    RunningProcess.on('exit', (code) => {
      if (code !== 0) {
        process.stderr.write('[' + new Date().toString() + '] ' + processName + ' Aborted.\n');
        reject(processName + ' Aborted.');
      }else {
        process.stderr.write('[' + new Date().toString() + '] ' + processName + ' Finished.\n');
        resolve(processName + ' Finished.');
      }
    });
  });
}
